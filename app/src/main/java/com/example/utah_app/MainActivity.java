package com.example.utah_app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    private String deviceName = null;
    private String deviceAddress;
    public static Handler handler;
    public static BluetoothSocket mmSocket;
    public static ConnectedThread connectedThread;
    public static CreateConnectThread createConnectThread;

    private final static int CONNECTING_STATUS = 1; // used in bluetooth handler to identify message status
    private final static int MESSAGE_READ = 2; // used in bluetooth handler to identify message update
    private final static int SENSOR_LEFT = 3;
    private final static int SENSOR_MIDDLE = 4;
    private final static int SENSOR_RIGHT = 5;
    private final static int SENSOR_REAR = 6;
    private final static int DRIVER_SPEED_CHANGE = 7;
    private final static int DRIVER_ANGLE_CHANGE = 8;
    private final static int MOTOR_CURRENT_SPEED = 9;
    //private final static int MOTOR_


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // UI Initialization
        final TextView sensorLeft = findViewById(R.id.left_sensor_value);
        final TextView sensorMiddle = findViewById(R.id.middle_sensor_value);
        final TextView sensorRight = findViewById(R.id.right_sensor_value);
        final TextView sensorRear = findViewById(R.id.rear_sensor_value);
        final TextView driverSpeed = findViewById(R.id. driver_speed_value);
        final TextView driverAngle = findViewById(R.id. driver_angle_value);
        final TextView motorSpeed = findViewById(R.id. motor_speed_value);

        final Button buttonConnect = findViewById(R.id.buttonConnect);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        final TextView textViewInfo = findViewById(R.id.textViewInfo);
        final Button buttonToggle = findViewById(R.id.buttonToggle);
        buttonToggle.setEnabled(false);
        final ImageView imageView = findViewById(R.id.imageView);
        imageView.setBackgroundColor(getResources().getColor(R.color.colorOff));

        // If a bluetooth device has been selected from SelectDeviceActivity
        deviceName = getIntent().getStringExtra("deviceName");
        if (deviceName != null){
            // Get the device address to make BT Connection
            deviceAddress = getIntent().getStringExtra("deviceAddress");
            // Show progress and connection status
            toolbar.setSubtitle("Connecting to " + deviceName + "...");
            progressBar.setVisibility(View.VISIBLE);
            buttonConnect.setEnabled(false);

            /*
            This is the most important piece of code. When "deviceName" is found
            the code will call a new thread to create a bluetooth connection to the
            selected device (see the thread code below)
             */
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            createConnectThread = new CreateConnectThread(bluetoothAdapter,deviceAddress);
            createConnectThread.start();
        }

        /*
        Second most important piece of Code. GUI Handler
         */
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg){
                switch (msg.what){
                    case CONNECTING_STATUS:
                        switch(msg.arg1){
                            case 1:
                                toolbar.setSubtitle("Connected to " + deviceName);
                                progressBar.setVisibility(View.GONE);
                                buttonConnect.setEnabled(true);
                                buttonToggle.setEnabled(true);
                                break;
                            case -1:
                                toolbar.setSubtitle("Device fails to connect");
                                progressBar.setVisibility(View.GONE);
                                buttonConnect.setEnabled(true);
                                break;
                        }
                        break;
                    //Left in as reference
                    case MESSAGE_READ:
                        String sj2Msg = msg.obj.toString(); // Read message from SJ2
                        switch (sj2Msg.toLowerCase()){
                            case "led is turned on":
                                imageView.setBackgroundColor(getResources().getColor(R.color.colorOn));
                                textViewInfo.setText("SJ2 Message : " + sj2Msg);
                                break;
                            case "led is turned off":
                                imageView.setBackgroundColor(getResources().getColor(R.color.colorOff));
                                textViewInfo.setText("SJ2 Message : " + sj2Msg);
                                break;
                        }
                        break;

                    case SENSOR_LEFT:
                        String sj2MsgL_Sensor = msg.obj.toString();

                        sensorLeft.setText(sj2MsgL_Sensor.substring(1,sj2MsgL_Sensor.length()));
                        break;
                    case SENSOR_MIDDLE:
                        String sj2MsgM_Sensor = msg.obj.toString();

                        sensorMiddle.setText(sj2MsgM_Sensor.substring(1,sj2MsgM_Sensor.length()));
                        break;
                    case SENSOR_RIGHT:
                        String sj2MsgRight_Sensor = msg.obj.toString();

                        sensorRight.setText(sj2MsgRight_Sensor.substring(1,sj2MsgRight_Sensor.length()));
                        break;
                    case SENSOR_REAR:
                        String sj2MsgRear_Sensor = msg.obj.toString();

                        sensorRear.setText(sj2MsgRear_Sensor.substring(1,sj2MsgRear_Sensor.length()));
                        break;
                    case DRIVER_SPEED_CHANGE:
                        String sj2speed_Driver = msg.obj.toString();

                        driverSpeed.setText(sj2speed_Driver.substring(1,sj2speed_Driver.length()));
                        break;
                    case DRIVER_ANGLE_CHANGE:
                        String sj2angle_Driver = msg.obj.toString();

                        driverAngle.setText(sj2angle_Driver.substring(1,sj2angle_Driver.length()));
                        break;
                    case MOTOR_CURRENT_SPEED:
                        String sj2speed_Motor = msg.obj.toString();

                        motorSpeed.setText(sj2speed_Motor.substring(1,sj2speed_Motor.length()));
                        break;



                }
            }
        };

        // Select Bluetooth Device
        buttonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Move to adapter list
                Intent intent = new Intent(MainActivity.this, SelectDeviceActivity.class);
                startActivity(intent);
            }
        });

        // Will close connection with board
        buttonToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectedThread.cancel();
                toolbar.setSubtitle("Device Disconnected");

            }
        });
    }

    /* ============================ Thread to Create Bluetooth Connection =================================== */
    public static class CreateConnectThread extends Thread {

        public CreateConnectThread(BluetoothAdapter bluetoothAdapter, String address) {
            /*
            Use a temporary object that is later assigned to mmSocket
            because mmSocket is final.
             */
            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(address);
            BluetoothSocket tmp = null;
            UUID uuid = bluetoothDevice.getUuids()[0].getUuid();

            try {
                /*
                Get a BluetoothSocket to connect with the given BluetoothDevice.
                Due to Android device varieties,the method below may not work fo different devices.
                You should try using other methods i.e. :
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                 */
                tmp = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(uuid);

            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            bluetoothAdapter.cancelDiscovery();
            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
                Log.e("Status", "Device connected");
                handler.obtainMessage(CONNECTING_STATUS, 1, -1).sendToTarget();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                    Log.e("Status", "Cannot connect to device");
                    handler.obtainMessage(CONNECTING_STATUS, -1, -1).sendToTarget();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            connectedThread = new ConnectedThread(mmSocket);
            connectedThread.run();
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

    /* =============================== Thread for Data Transfer =========================================== */
    public static class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream

            int bytes = 0; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    /*
                    Read from the InputStream from SJ2 until termination character is reached.
                    Then send the whole String message to GUI Handler.
                     */

                    buffer[bytes] = (byte) mmInStream.read();
                    String readMessage;
                    if (buffer[bytes] == '\n'){
                        readMessage = new String(buffer,0,bytes);
                        int readMessageLength = readMessage.length();
                        Log.e("SJ2 Message",readMessage);
                        switch (readMessage.charAt(0)){
                            case '3':
                                handler.obtainMessage(SENSOR_LEFT,readMessage).sendToTarget();
                                break;
                            case '4':
                                handler.obtainMessage(SENSOR_MIDDLE,readMessage).sendToTarget();
                                break;
                            case '5':
                                handler.obtainMessage(SENSOR_RIGHT,readMessage).sendToTarget();
                                break;
                            case '6':
                                handler.obtainMessage(SENSOR_REAR,readMessage).sendToTarget();
                                break;
                            case '7':
                                handler.obtainMessage(DRIVER_SPEED_CHANGE,readMessage).sendToTarget();
                                break;
                            case '8':
                                handler.obtainMessage(DRIVER_ANGLE_CHANGE,readMessage).sendToTarget();
                                break;
                            case '9':
                                handler.obtainMessage(MOTOR_CURRENT_SPEED,readMessage).sendToTarget();
                                break;
                            default:
                                handler.obtainMessage(MESSAGE_READ,readMessage).sendToTarget();
                        }

                        bytes = 0;
                        for(int i =0; i < 20; i++){
                            buffer[i] = 0;
                        }
                    } else {
                        bytes++;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String input) {
            byte[] bytes = input.getBytes(); //converts entered String into bytes
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
                Log.e("Send Error","Unable to send message",e);
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    /* ============================ Terminate Connection at BackPress ====================== */
    @Override
    public void onBackPressed() {
        // Terminate Bluetooth Connection and close app
        if (createConnectThread != null){
            createConnectThread.cancel();
        }
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
}